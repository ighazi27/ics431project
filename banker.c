//
//  banker.c
//
//
//  Created by Osama Al-Najjar, Ghazi Al-Ghamdi, and Mohammed Al-Sha'alan on 11/25/15.
//  This code was created for the Operating System Course (ICS431) for the Lab project.
//
//
//  The below algorithm was impleneted to simulate bankers algorithm for a number of customers accessing the bank..
//      .. system to make some transactions (allocate resources in system).
//  The customers requests are simulated by using multi threads requests to the OS to allocate resources to each customer.
//  Safe check is also implemented to make sure that !@!TODO!@! add_description here.
//
//  P.S: When you compile, you need to use "gcc banker.c -lpthread" because the library pthread.h is used.
//
//
//#include <>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>


/*
 * Initialize CONSTANTS.
 *
 * - MAX                    The maximum random number generated for setting up the maximum array.
 * - NUMBER_OF_CUSTOMERS    The numbers of customers using the system. --> process
 * - NUMBER_OF_RESOURCES    The number of resources available in the system. --> resources
 */

#define NUMBER_OF_CUSTOMERS 3
#define NUMBER_OF_RESOURCES 5
int MAX;

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// Functions Headers, to avoid warning messages,
// Please put a header for each newly created function.
int request_resources(int customer_number, int requested_resources[]);
int pretend_granting_request(int customer_number, int requested_resources[]);
int release_resources(int customer_number, int release_resources[]);
int grant_resources(int customer_number,int requested_resources []);
int validity_check(int customer_number, int requested_resources[]);
int request_LTOE_available(int customer_number, int requested_resources[]);
int request_LTOE_need(int customer_number, int requested_resources[]);
void update_state(int available[NUMBER_OF_CUSTOMERS], int maximum[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES],
                  int allocation[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES],
                  int need[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES]);

void set_up_maximum();
int zeros_check(int resources[]);

// Customers threads simulation functions
void *customer_run(void *customer_id);
void customers_threads ();

//
void alarmHandler();


/*
 * Initialize instance variables.
 * 
 * - available  Array stores the number of resources available to be allocated to customers. [Resources] 
 * - maximum    Matrix that stores the maximum resources needed for each customer before he starts. [Process][Resource]
 * - allocation Matrix that has the current allocation of resources to customers.[Process][Resource]
 * - need       Matrix that has current need of resources to each customer.[Process][Resource], need = max - allocation
 * - semaphore  Semaphore that will make the process wait and post.
 */
int available[NUMBER_OF_RESOURCES];
int maximum[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];
int allocation[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];
int need[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES];
sem_t semaphore;



/*
 * !@!TODO!@! add_description here.
 */
int main(int argc, char *avail[])
{ 
    // !@!TODO!@! Sh3lan please review this!

    // Check if the number of available resources is provided e.g ./a.out {#resource1} {#resource2} {#resource3} {MAX: maximum need}
    if ( argc != (NUMBER_OF_RESOURCES + 2 ))
    {
        printf("Error: Provide %d integers to specify the number of instances for each resource type, followed by a value for MAX \n", NUMBER_OF_RESOURCES);
        exit(1);
        //!@!TODO!@! Validate the number of resources. e.g. not more than 1M nor less than 0
    }
    //populate available[]
    int rsc ;
    printf("\nAvailable:\n");
    for ( rsc = 0 ; rsc < NUMBER_OF_RESOURCES ; rsc++){
        available[rsc] = atoi(avail[rsc + 1]);
        printf("%d\t",available[rsc]);
    }
    printf("\n_____________________\n");
    
    MAX = atoi(avail[NUMBER_OF_RESOURCES + 1]);
    set_up_maximum();
    // Initialize the semaphore.
    sem_init(&semaphore, 0, 0);
    sem_post(&semaphore);
    
    signal(SIGTSTP, alarmHandler);

    // Generate the customer threads.
    customers_threads();
}

void alarmHandler(){
    int rsc ;
    printf("\n_____________________\n");
    printf("\nAvailable:\n");
    for ( rsc = 0 ; rsc < NUMBER_OF_RESOURCES ; rsc++){
        
        printf("%d\t",available[rsc]);
    }
    printf("\n_____________________\n");
    int cstr;

    printf("\nNeed array:\n");
    //loop through the customers
    for ( cstr = 0 ; cstr < NUMBER_OF_CUSTOMERS; cstr++){
        printf("\nCustomer #%d:\t", (cstr));
        
        //loop through the resources
        int rsc;
        for ( rsc = 0 ; rsc < NUMBER_OF_RESOURCES; rsc++){
            
            printf("%d\t",need[cstr][rsc]);
        }// end for (resources)
    }// end for (customers)
    printf("\n_____________________\n");
    
}

void set_up_maximum(){
    
    srand(time(NULL));
    int cstr;
    printf("\nMaximum array:\n");
    //loop through the customers
    for ( cstr = 0 ; cstr < NUMBER_OF_CUSTOMERS; cstr++){
        printf("\nCustomer #%d:\t", (cstr));

        //loop through the resources
        int rsc;
        for ( rsc = 0 ; rsc < NUMBER_OF_RESOURCES; rsc++){
            //generate random number maximum[customer][resource] less than the available[customer]
            maximum[cstr][rsc] = rand() % MAX;
            // Initially, need = max
            need[cstr][rsc] = maximum[cstr][rsc];
            
            printf("%d\t",maximum[cstr][rsc]);
        }// end for (resources)
    }// end for (customers)
    printf("\n_____________________\n");
}

/*
 * The customer calls this function to request a resource from the banker.
 * Before the customer requests a resource, the function checks for its validity and makes the safety check.
 * return :
 * 0 : the banker denied the request.
 * 1 : the method completed succesfully.
 */
int request_resources(int customer_number, int requested_resources[])
{
	//return 1; // REMOVE this line when you finish all functions below!

    // Check for the validity of the request.
    if (validity_check(customer_number, requested_resources))
    {
        sem_wait(&semaphore);
        
        int returnedValue;
        
        // Request is valid, check if it will lead to a not safe state -- DeadLock might happen --.
        if (pretend_granting_request(customer_number, requested_resources))
        {
            // Request is Valid, And will not lead to a not safe state.
            // Grant requested resources to the requesting process THEN move to the new state.
            returnedValue = grant_resources(customer_number, requested_resources);
        }
        else
        {
            // Request is valid, but it will lead to a not safe state
            returnedValue = 0;
        }
        //printNeed();
        // Signal the semaphore.
        sem_post(&semaphore);
        
        return returnedValue;
    }
    else
    {
        // Request is not valid
        return 0;
    }
}


/*
 * !@!TODO!@! add_description here.
 */
int pretend_granting_request(int customer_number, int requested_resources[])
{
    //oss
    // Initialize the iterators.
    int i = 0;
    int j = 0;
    int k = 0;
    int l = 0;
    int m = 0;
    
    // Initialize the work array.
    int work[NUMBER_OF_RESOURCES];
    
    // Initialize the checked customers.
    int finish[NUMBER_OF_CUSTOMERS]; // 1 = finished. 0 = not finished --> default.
    for(i = 0; i < NUMBER_OF_CUSTOMERS; i++)
        finish[i] = 0;
    
    // Copy the available array into the work array.
    for(i = 0; i < NUMBER_OF_RESOURCES; i++)
        work[i] = available[i];
    
    // Initialize the counters.
    int hitCount;       // number of customers that finished in each loop.
    int canFinish;      // 0 = can't finish. 1 = can finish.
    int safe;           // 0 = not safe. 1 = safe state.
    
    while(1){
        
        // If the loop of all customers have no hits, then the state is not safe.
        hitCount = 0;
        for(i = 0; i < NUMBER_OF_CUSTOMERS; i++){
            
            if(finish[i] == 1)
                continue;
            
            // Check the resources need for customer i.
            // Assume customer will finish, otherwise change to 0 in the inner loop.
            canFinish = 1;
            
            //for(j = 0; j < NUMBER_OF_RESOURCES; j++){
            //    if(need[i][j] > work[j])
            //        canFinish = 0;
            //}
            
            // If customer i has all recources less than or equal than its need in work then it can finish
            if(canFinish == 1){
                finish[i] = 1;
                hitCount++;
                
                // Add to work the allocation of Process i who has finished.
                for(l = 0; l < NUMBER_OF_RESOURCES; l++)
                    work[l] += allocation[i][l];
            }
            
        }
        
        // Check if all customers have finished, then safe will still be = 1. unless one customer or more not finished then safe = 0 --> not safe.
        safe = 1;
        for(m = 0; m < NUMBER_OF_CUSTOMERS; m++){
            // If one customer not finished --> 0, then the state is not safe yet.
            if(finish[m] == 0)
                safe = 0;
        }
        
        // State is still not safe and one or more customers could not finish in the loop, no hits.
        if(hitCount == 0 && safe == 0){
            
            printf(ANSI_COLOR_RED "NOT safe - ");
            return 0;
        }
        // The state is safe
        if(safe == 1){
            //printf("SAFE\n");
            return 1;
        
        }
    }
    return 1;   
}



/*
 * Release resource from a customer request after they are done excecuting.
 * return :
 * 1 : the method completed excecuting.
 */
int release_resources(int customer_number, int release_resources[])
{
    
    if(zeros_check(release_resources) == 0){
        return 0;
    }
    
    sem_wait(&semaphore);
    int released = 0;
    // Loop through the resources and update them.
	int i;
    for ( i = 0; i < NUMBER_OF_RESOURCES; i++)
    {
        if (allocation[customer_number][i] > 0 && allocation[customer_number][i] >= release_resources[i] ) {
            // Update the allocation array by removing from it the finished resources.
            allocation[customer_number][i] -= release_resources[i];
            // Increase the available amount of resources after they finished.
            available[i] += release_resources[i];
            
            released = 1;
            //printf(ANSI_COLOR_MAGENTA " %d", release_resources[i]);
        }

    }
    // Signal the semaphore.
    sem_post(&semaphore);
    if (released) {
        
        printf(ANSI_COLOR_MAGENTA "R(%d) ", customer_number);
        return 1;
    }
    return 0;
}



/*
 * Grant resources to the customer request.
 * return :
 * 1 : the method completed excecuting.
 */
int grant_resources(int customer_number, int requested_resources[])
{

    // Loop through the resources and update them.
	int i;
    for ( i = 0; i < NUMBER_OF_RESOURCES; i++)
    {
        // Decrement the customer need by the requested amount of resources.
        need[customer_number][i] -= requested_resources[i];
        
        // Update the allocation array by adding to it the new allocated resources.
        allocation[customer_number][i] += requested_resources[i];
        
        // Decrement the available amount of resources after they are granted.
        available[i] -= requested_resources[i];
        //printf(ANSI_COLOR_GREEN " %d", requested_resources[i]);

    }
    printf(ANSI_COLOR_GREEN "G(%d) ", customer_number);
    //printf("Request granted for customer #%d\n", customer_number);

    return 1;
}



/*
 * Check if request from customer_number is valid.
 * return :
 * 0 : requested_resources are bigger than the (need[customer_number] && avaliable)
 * 1 : requested_resources are less than or equal to the (need[customer_number] && avaliable)
 */
int validity_check(int customer_number, int requested_resources[])
{
    
    if (zeros_check(requested_resources)
        &&
        request_LTOE_need(customer_number, requested_resources)
         &&
         request_LTOE_available(customer_number, requested_resources)
       ) 
    {
        return 1;
    }
  
    return 0;
}

/*
 * Check if the customer {process} is requesting/releasing zeros.
 * return :
 * 0 : all resources[] are zeros
 * 1 : resources[] are not zeros
 */
int zeros_check(int resources[]){
    int i;
    for ( i = 0; i < NUMBER_OF_RESOURCES; i++)
    {
        if (resources[i] > 0)
        {
            return 1;
        }
    }
    return 0;
}

/*
 * Check if the customer {process} request is less than or
 * equal its need LTOE = less than or equal.
 * return :
 * 0 : requested_resources[] from customer_number is bigger than the need[customer_number]
 * 1 : requested_resources[] from customer_number is less than or equal the need[customer_number]
 */
int request_LTOE_need(int customer_number, int requested_resources[])
{
    int i ;
    for ( i = 0; i < NUMBER_OF_RESOURCES; i++)
    {
        if (need[customer_number][i] < requested_resources[i])
        {
            return 0;
        }
    }
    return 1;

}



/*
 * Check if the customer {process} request is less than or
 * equal the available resources LTOE = less than or equal.
 * return :
 * 0 : requested_resources[] from customer_number is bigger than the available[]
 * 1 : requested_resources[] from customer_number is less than or equal the available[]
 */
int request_LTOE_available(int customer_number, int requested_resources[])
{
    int i ;
    for ( i = 0; i < NUMBER_OF_RESOURCES; i++)
    {
        if (available[i] < requested_resources[i])
        {
            return 0;
        }
    }
    return 1;

}



void update_state(int available[NUMBER_OF_CUSTOMERS], int maximum[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES],
                  int allocation[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES],
                  int need[NUMBER_OF_CUSTOMERS][NUMBER_OF_RESOURCES])
{

    // Please suggest a better header e.g. parameters
    // what do we need to update the current state ?
    // keep in mind that we will use this function to 
    // get the the new state when pretending granting 
    // a request.
    // My Suggestion is to have a struct called State,
    // which will capture and have all state related 
    // data, including but not limited to : 
    // 1. available[]
    // 2. maximum[][]
    // 3. allocation[][]
    // 4. need[][]
    // and this function will return a {State}.
}



void *customer_run(void *customer_id)
{
	int my_id = (int)customer_id;
	//printf(ANSI_COLOR_RESET"\n\tHi, I am Customer #%d\n", my_id);
	
	//int myMax[NUMBER_OF_RESOURCES];

	//int myNeed[NUMBER_OF_RESOURCES] = myMax;

	//int myAllocation[NUMBER_OF_RESOURCES];
	srand(time(NULL));

	int resource_counter;
	int a_request[NUMBER_OF_RESOURCES];
	int a_release[NUMBER_OF_RESOURCES];
	int myNeed;
	int I_am_done; // customer finish indicator
	int r = 10;
	while(1== 1){
        	r--;
		I_am_done = 1;
        
		// Generate a valid request
		for(resource_counter=0 ; resource_counter < NUMBER_OF_RESOURCES ; resource_counter++)
		{
			myNeed = need[my_id][resource_counter]; // customer need for single resource.

			if(myNeed > 0)
			{
				// the customer still have need (not done yet)
				I_am_done = 0; 
                
                //srand(time(NULL));
                
				// Generate request with random_number less than or equal to my need + 1.
                int random_number = rand() % ( need[my_id][resource_counter] + 1 );
				a_request[resource_counter] = random_number;
                		// Generate release values with random_number less than or equal to my current allocation.
				random_number = rand() % ( allocation[my_id][resource_counter] + 1 );
				a_release[resource_counter] = random_number;
			}
			else
			{   //the customer has need = 0 for this resource
				a_request[resource_counter] = 0;
			}
		} // end for (Resources loop)
		
		if(I_am_done)
		{
			// the customer have consumed all his needs

			printf(ANSI_COLOR_YELLOW "\nCustomer #%d is broke! Going out of the bank now.\n", my_id);
			pthread_exit(NULL);
		}
		else
		{ 
			
			if(request_resources(my_id, a_request))
			{
                
				//printf(ANSI_COLOR_GREEN "+%d ", my_id);

			}
			else
			{
				//printf("I am Customer #%d and my request is denied :( \n", my_id);
			}

			release_resources(my_id, a_release);	//random release
		
		}
	
	} // end while

}


/*
 * This function creates the threads for each customer.
 * Each customer will request and release random reasources.
 * The request is less than the need and the release is less than the allocation.
 */
void customers_threads (){

	pthread_t customer_thread[NUMBER_OF_CUSTOMERS]; 
	int customers_count;
	for(customers_count = 0; customers_count < NUMBER_OF_CUSTOMERS; customers_count++)
	{
		//printf(ANSI_COLOR_RESET "\nCreating a thread for cutomer #%d\n", (customers_count) );

		pthread_create(&customer_thread[customers_count], NULL, customer_run, (void *)customers_count);
	}
    
    for(customers_count = 0; customers_count < NUMBER_OF_CUSTOMERS; customers_count++)
    {
        pthread_join(customer_thread[customers_count], NULL);
        
    }
    

	printf(ANSI_COLOR_CYAN "\n\nI am the BANKER. All customers are broke! The Bank is closing now\n\n" ANSI_COLOR_RESET "\n");
    //pthread_exit(NULL);
}
